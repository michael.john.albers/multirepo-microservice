# Based on https://stackoverflow.com/questions/53835198/integrating-python-poetry-with-docker
# Skipped using alpine/pip as it required gcc, rust and other packages. Installing with Debian
# based image was much simpler

FROM python:3.6.14-slim-buster

ARG BUILD_TYPE
ARG PIP_ARGS
ARG POETRY_VERSION=1.1.8

ENV BUILD_TYPE=${BUILD_TYPE} \
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100

RUN apt-get update && apt-get install -y curl

RUN python3 -m pip install $PIP_ARGS --user pipx && \
    python3 -m pipx ensurepath
ENV PATH="${PATH}:/root/.local/bin"
# This is not the preferred method to install Poetry. However, the preferred way will not work on the high side.
# Using pipx as installing with normal pip can cause collisions between Poetry's dependencies and app dependencies
RUN pipx install --pip-args="$PIP_ARGS" "poetry==$POETRY_VERSION"

# Copy only requirements to cache them in docker layer
WORKDIR /code
COPY poetry.lock pyproject.toml /code/

# Project initialization
RUN poetry config virtualenvs.create false \
  && poetry install $(test "$BUILD_TYPE" = production && echo "--no-dev") --no-interaction --no-ansi

# Creating folders, and files for a project:
COPY . /code

ENTRYPOINT ["poetry", "run", "python", "chipper/main.py"]
